# Identification of the funding scheme

There are plenty of national and international funding programmes. Quite often you will easily find a programme which fits your idea best. If this is not the case, there is still a good chance to write a winning proposal. You will find out how this can be done in the following chapter.

## Funding programmes

National and international funding programmes differ in many aspects:
- Objectives
- Themes and topics
- Eligibility criteria (who may apply or receive funding)
- Funding rules
- Duration of funding
- Funding limits
- Evaluation process 
- Contractual rules

However, all funding programmes can be sorted according to how the topics of research projects are defined:
- If the topic of research is described in detail in the call for proposals, the funding programme is organised top-down. The officials define the research challenge and the anticipated results, which should be achieved by the project. Sometimes also the favoured way of solving the challenge will be narrowed down. In this case your project needs to propose a solution to the challenge, which will achieve or even excel the anticipated outcomes (Figure 2 left side). 
- The other option to organise a funding programme is to define a research or application area and to ask researchers to propose interesting and attractive research projects. As the applicant takes over the expert role of defining what exactly is relevant in his or her field of research, this process is called bottom-up (Figure 2 right side).

 	![Top-down, bottom-up organisation of funding programmes](images/fp_top-down-bottom-up.jpg)

For designing a top-down programme, funding agencies involve external experts. In this process lobbyists have taken an influence on the objectives of the different calls of these programmes. For you as the person writing the proposal, this has an advantage: The reviewers have been instructed before, which topics the funding authority considers attractive for funding and you do not have to justify this anymore. However, sometimes individual calls seem to expect a specific proposal by a specific consortium. Therefore, the challenge for a successful proposal lies in deftly answering to the expectations which are defined in the respective call for proposals.
In contrast, bottom-up programmes allow you to freely define your research objectives, solutions and impact of projects. The challenge here lies in persuading the reviewers of the importance and attractiveness of the proposed research topic. 
The following questions help you to identify what kind of programme you are looking for when screening for suitable funding opportunities:
- Can I do my research project all by myself? Look out for individual fellowships.
- Is it basic research? Look out for funding programmes targeting basic research.
- Is it applied research? Look out for funding programmes requiring the participation of enterprises. 
- Do I need only national organisations to implement the project? Look out on national level.
- Do I need to collaborate internationally? Look out for funding targeting international collaboration.
- Is there a top-down call, to which I can easily fit my project?
- If there is no top-down call, which bottom-up call is suitable?
